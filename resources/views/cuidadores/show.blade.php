@extends('layouts.master')
@section('titulo')
Mostrar Cuidador
@endsection
@section('contenido')
<div class="row">
    <div class="col-sm-3">
        Cuidador
    </div>
    <div class="col-sm-9">
        <p class="h1">{{$cuidador->nombre}}</p>
        <p class="h4">Titulaciones:</p>
        <ul>
            @foreach($cuidador->titulaciones() as $titulacion)
            <li>Titulación: {{$titulacion->nombre}}</li>
            @endforeach
        </ul>
        
           
            <a href="{{ route('animales.index') }}"><button class="btn btn-dark">Mostrar todos los animales</button></a>
            
    </div>
</div>

</div>
</div>
@endsection