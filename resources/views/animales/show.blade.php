@extends('layouts.master')
@section('titulo')
Mostrar animal
@endsection
@section('contenido')
<div class="row">
    <div class="col-sm-3">
        <img src="{{asset('')}}/{{$animal->imagen}}" class="img-thumbnail" alt="{{$animal->especie}}">
    </div>
    <div class="col-sm-9">
        <p class="h1">{{$animal->especie}} ({{$animal->getEdad()}} años)</p>
        <p class="h4">Peso:</p>
        <p class="lead">{{$animal->peso}} kg</p>
        <p class="h4">Altura:</p>
        <p class="lead">{{$animal->altura}} m</p>
        <p class="h4">Descripción:</p>
        <p class="lead">{{$animal->descripcion}}</p>
        <p class="h4">Revisiones:</p>
        <ul>
            @foreach($animal->revisiones as $revision)
            <li>Fecha: {{$revision->fecha}} -- Revision numero: {{$revision->id}} -- Descripcion: {{$revision->descripcion}}</li>
            @endforeach
        </ul>
        
            <br>
            <br>
            <a href="{{ route('animales.edit', $animal) }}"><button class="btn btn-warning">Editar</button></a>
            <a href="{{ route('revisiones.create', $animal)}}"><button type="button" class="btn btn-success">Añadir revisión</button></a>
            <a href="{{ route('animales.index') }}"><button class="btn btn-dark">Mostrar todos los animales</button></a>
            
    </div>
</div>

</div>
</div>
@endsection