<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
    <script src="{{ url('/assets/bootstrap/js/bootstrap.min.js') }}"></script>
    <link href="{{ url('/assets/bootstrap/css/estilo.css') }}" rel="stylesheet" type="text/css" >
    <script type="text/javascript" src="{{url("/assets/js/jquery-3.5.1.min.js")}}"></script>
    <script type="text/javascript" src="{{url("/assets/js/jquery-ui.js")}}"></script>
    <link rel="stylesheet" href="{{url("/assets/css/jquery-ui.css")}}">
    <title>@yield('titulo')</title>
</head>

<body>
    @include('layouts.partials.navbar')

    <div class="container-fluid">
        @yield('contenido')
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>
 
</body>

</html>