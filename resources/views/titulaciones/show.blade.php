@extends('layouts.master')

@section('titulo')
    Zoologico
@endsection

@section('contenido')
<div class="row">
    <div class="col-sm-9">

       @foreach ($titulacion->cuidadores as $titulo)
           <ul>
               <li>Nombre: {{$titulo->nombre}}</li>
           </ul>
        @endforeach

        <div>
            <a href="{{route('animales.index')}}"><button type="button" class="btn btn-secondary">Volver al Listado de Animales</button></a>

    </div>
    </div>
@endsection
