<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $u = new User();

        $u->name = "Fernando";
        $u->email = "fernando.palaciogomez@iesmiguelherrero.com";
        $u->password = bcrypt("prueba");

        $u2 = new User();

        $u2->name = "Juan";
        $u2->email = "juan.apellidoapellido@iesmiguelherrero.com";
        $u2->password = bcrypt("prueba2");

        $u->save();
        $u2->save();

        $this->command->info("Se han añadido dos usuarios predeterminados y 5 aleatorios");
    }
}
