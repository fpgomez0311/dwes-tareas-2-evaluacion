<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Revision;
use App\Models\Animal;

class RevisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $r=new Revision();
        $r->animal_id=Animal::all()->random()->id;
        $r->fecha="2021/05/24";
        $r->descripcion="Revisión de prueba 1";

        $r2=new Revision();
        $r2->animal_id=Animal::all()->random()->id;
        $r2->fecha="2021/05/24";
        $r2->descripcion="Revisión de prueba 2";

        $r->save();
        $r2->save();

        $this->command->info('Tabla revisiones inicializada con datos');
    }
}
