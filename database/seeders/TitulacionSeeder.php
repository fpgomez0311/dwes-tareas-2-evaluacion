<?php

namespace Database\Seeders;

use App\Models\Titulacion;
use Illuminate\Database\Seeder;

class TitulacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $t1 = new Titulacion();
        $t1->id = 1;
        $t1->nombre = "Licenciado";
        $t1->slug = "licenciado";
        $t1->save();

        $t2 = new Titulacion();
        $t2->id = 2;
        $t2->nombre = "Grado en Veterinária";
        $t2->slug = "grado-en-veterinaria";
        $t2->save();

        $t3 = new Titulacion();
        $t3->id = 3;
        $t3->nombre = "Zoología";
        $t3->slug = "zoologia";
        $t3->save();
        
        $this->command->info('Tabla titulaciones inicializada con datos');

    }
}
