<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Animal extends Model
{
    use HasFactory;
    protected $table = "animales";
    protected $guarded = [];

    public function getEdad()
    {
        $fechaFormateada = Carbon::parse($this->fechaNac);
        return $fechaFormateada->diffInYears(Carbon::now());
    }
    public function revisiones()
    {
        return $this->hasMany(Revision::class);
    }
    public function getRouteKeyName()
{
 return 'slug';
}
public function cuidadores()
 {
 return $this->belongsToMany(Cuidador::class);
 }

}
